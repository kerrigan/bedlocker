package bed.rotate.locker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

public class RotationAlarmReceiver extends BroadcastReceiver {
	public final static String ORIENTATION = "orientation";

	public RotationAlarmReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		int rotation = intent.getExtras().getInt(ORIENTATION);

		/*
		if (rotation == RotateAlarm.ORIENTATION_AUTO) {
			Utils.setAutoOrientation(context, true);
		} else {
			Utils.setAutoOrientation(context, false);
			int systemOrientation = -1;
			switch (rotation) {
			case RotateAlarm.ORIENTATION_PORTRAIT:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;
			case RotateAlarm.ORIENTATION_LANDSCAPE:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				break;
			case RotateAlarm.ORIENTATION_LANDSCAPE_REVERSE:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			case RotateAlarm.ORIENTATION_PORTRAIT_REVERSE:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
			}
			if(systemOrientation != -1){
				Utils.changeOrientation(context, systemOrientation);
			}
		}
		*/
		
		switch (rotation) {
		case RotateAlarm.ORIENTATION_AUTO:
			Utils.setAutoOrientation(context, true);
			break;

		case RotateAlarm.ORIENTATION_FIXED:
			Utils.setAutoOrientation(context, false);
			break;
		}
	}
}
