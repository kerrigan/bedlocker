package bed.rotate.locker;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TimePicker;

import com.actionbarsherlock.app.SherlockDialogFragment;

public class AlarmDialogFragment extends SherlockDialogFragment {
	public static final String ALARM_TIME = "alarm_time";
	public static final String ORIENTATION = "orientation";
	public static final String INTENT_ID = "intent_id";
	
	private TimePicker mTpAlarm;
	private RadioGroup mRgOrientations;
	private Button mBtnConfirm;
	
	private int mCurrentOrientation = RotateAlarm.ORIENTATION_AUTO;
	private boolean isNew = true;
	private int mCurrentIntentId = -1;
	
	public interface DialogConfirmListener {
		void onConfirmDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle bundle = getArguments();

		getDialog().setTitle("Edit orientation change");
		View v = inflater.inflate(R.layout.dialog_add_rotate_alarm, null);
		mTpAlarm = (TimePicker) v.findViewById(R.id.tpAlarm);
		mTpAlarm.setIs24HourView(true);

		
		mRgOrientations = (RadioGroup)v.findViewById(R.id.rgOrientations);
		mBtnConfirm = (Button)v.findViewById(R.id.btnConfirm);

		Calendar cal = Calendar.getInstance();
		if(bundle != null){
			isNew = false;
			Date date = new Date(bundle.getLong(ALARM_TIME));

			cal.setTime(date);

			mCurrentIntentId = bundle.getInt(INTENT_ID);
			
			switch (bundle.getInt(ORIENTATION)) {
			case RotateAlarm.ORIENTATION_PORTRAIT:
				mRgOrientations.check(R.id.radioPortrait);
				break;
			case RotateAlarm.ORIENTATION_LANDSCAPE:
				mRgOrientations.check(R.id.radioLandscape);
				break;
			case RotateAlarm.ORIENTATION_AUTO:
				mRgOrientations.check(R.id.radioAuto);
				break;
			case RotateAlarm.ORIENTATION_FIXED:
				mRgOrientations.check(R.id.radioFixed);
				break;
				/*
			case RotateAlarm.ORIENTATION_LANDSCAPE_REVERSE:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			case RotateAlarm.ORIENTATION_PORTRAIT_REVERSE:
				systemOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
				*/
			}			 
		}
		
		mTpAlarm.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
		mTpAlarm.setCurrentMinute(cal.get(Calendar.MINUTE));
		
		mRgOrientations.setOnCheckedChangeListener(mRadioGroupListener);
		mBtnConfirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				saveAlarm();
				
				((DialogConfirmListener)getActivity()).onConfirmDialog();
				dismiss();
			}
		});
		return v;
	}
	
	protected void saveAlarm() {
		DataStore settings = DataStore.getInstance(getActivity());
		
		RotateAlarm newAlarm = new RotateAlarm();
		newAlarm.alarmIntentId = settings.getNextId();
		newAlarm.orientation = mCurrentOrientation;
		Calendar cal = Calendar.getInstance(TimeZone.getDefault());
		
		int minute = mTpAlarm.getCurrentMinute();
		int hour = mTpAlarm.getCurrentHour();
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		newAlarm.timeInMillis = cal.getTimeInMillis();
		
		Date d = new Date(newAlarm.timeInMillis);
		Log.i("Tag", newAlarm.timeInMillis + "");
		//Log.i("tag", d.getHours() + ":" + d.getMinutes());
		
		List<RotateAlarm> alarms = settings.getAlarms();
		
		if(isNew){
			alarms.add(newAlarm);
		} else {
			RotateAlarm alarmToReplace;
			int alarmIndex = -1;
			for (int i = 0; i < alarms.size(); ++i) {
				alarmToReplace = alarms.get(i);
				if(alarmToReplace.alarmIntentId == mCurrentIntentId){
					alarmIndex = i;
					break;
				}
			}
			
			Utils.removeAlarm(getActivity(), mCurrentIntentId);
			
			if(alarmIndex != -1){
				alarms.set(alarmIndex, newAlarm);
			}
		}
		settings.storeAlarms(alarms);
		
		Utils.setAlarm(getActivity(), newAlarm.timeInMillis, mCurrentOrientation, newAlarm.alarmIntentId);
	}

	private final OnCheckedChangeListener mRadioGroupListener = new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch (checkedId) {
			case R.id.radioAuto:
				mCurrentOrientation = RotateAlarm.ORIENTATION_AUTO;
				break;
			case R.id.radioFixed:
				mCurrentOrientation = RotateAlarm.ORIENTATION_FIXED;
				break;
				/*
			case R.id.radioLandscape:
				mCurrentOrientation = RotateAlarm.ORIENTATION_LANDSCAPE;
				break;
			case R.id.radioPortrait:
				mCurrentOrientation = RotateAlarm.ORIENTATION_PORTRAIT;
				break;
			case R.id.radioReversePortrait:
				mCurrentOrientation = RotateAlarm.ORIENTATION_PORTRAIT_REVERSE;
				break;
			case R.id.radioReverseLandscape:
				mCurrentOrientation = RotateAlarm.ORIENTATION_LANDSCAPE_REVERSE;
				break;
				*/
			}
		}
	};
}
