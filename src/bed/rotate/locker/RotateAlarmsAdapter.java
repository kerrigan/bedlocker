package bed.rotate.locker;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RotateAlarmsAdapter extends ArrayAdapter<RotateAlarm> {

	public RotateAlarmsAdapter(Context context) {
		super(context, 0);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final RotateAlarm alarm = getItem(position);
		View v = convertView;
		if (v == null || !(v instanceof RelativeLayout)) {
			LayoutInflater vi = LayoutInflater.from(getContext());
			v = vi.inflate(R.layout.rotatealarm, null);

			ViewHolder holder = new ViewHolder();
			holder.txtTime = (TextView) v.findViewById(R.id.txtTime);
			holder.imgOrientation = (ImageView) v.findViewById(R.id.imgOrientation);
			holder.imgDelete = (ImageView) v.findViewById(R.id.imgDelete);
			v.setTag(holder);
		}

		ViewHolder holder = (ViewHolder) v.getTag();
		
		switch (alarm.orientation) {
		case RotateAlarm.ORIENTATION_AUTO:
			holder.imgOrientation.setImageResource(R.drawable.ic_auto);
			break;
		case RotateAlarm.ORIENTATION_FIXED:
			holder.imgOrientation.setImageResource(R.drawable.ic_portrait);
			break;
			/*
		case RotateAlarm.ORIENTATION_PORTRAIT:
			holder.imgOrientation.setImageResource(R.drawable.ic_portrait);
			break;
		case RotateAlarm.ORIENTATION_LANDSCAPE:
			holder.imgOrientation.setImageResource(R.drawable.ic_landscape);
			break;
		case RotateAlarm.ORIENTATION_PORTRAIT_REVERSE:
			holder.imgOrientation.setImageResource(R.drawable.ic_reverse_portrait);
			break;
		case RotateAlarm.ORIENTATION_LANDSCAPE_REVERSE:
			holder.imgOrientation.setImageResource(R.drawable.ic_reverse_landscape);
			break;
			*/
		}
		
		
		Date date = new Date(alarm.timeInMillis);
		Calendar cal = Calendar.getInstance(TimeZone.getDefault());
		cal.setTime(date);
		holder.txtTime.setText(String.format("%02d:%02d",
				cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
		holder.txtTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlarmDialogFragment df = new AlarmDialogFragment();
				
				Bundle args = new Bundle(2);
				
				args.putLong(AlarmDialogFragment.ALARM_TIME, alarm.timeInMillis);
				args.putInt(AlarmDialogFragment.INTENT_ID, alarm.alarmIntentId);
				args.putInt(AlarmDialogFragment.ORIENTATION, alarm.orientation);
				
				df.setArguments(args);
				
				df.show(((SherlockFragmentActivity)getContext())
						.getSupportFragmentManager(), "dialog");
			}
		});
		
		holder.imgDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DataStore settings = DataStore.getInstance(getContext());
				List<RotateAlarm> currentAlarms = settings.getAlarms();
				Utils.removeAlarm(getContext(), alarm.alarmIntentId);
				currentAlarms.remove(position);
				settings.storeAlarms(currentAlarms);
				remove(alarm);
				notifyDataSetChanged();
			}
		});

		return v;
	}

	static class ViewHolder {
		TextView txtTime;
		ImageView imgOrientation;
		ImageView imgDelete;
	}

}