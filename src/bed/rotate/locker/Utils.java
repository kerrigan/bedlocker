package bed.rotate.locker;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;

public class Utils {
	public static void changeOrientation(Context context, int orientation){
		//Меняет системную ориентацию
		WindowManager wm = (WindowManager) context.getSystemService(Service.WINDOW_SERVICE);

	    LinearLayout orientationChanger = new LinearLayout(context);
	    orientationChanger.setClickable(false);
	    orientationChanger.setFocusable(false);
	    orientationChanger.setFocusableInTouchMode(false);
	    orientationChanger.setLongClickable(false);
	    
	    
		LayoutParams orientationLayout = new WindowManager.LayoutParams(
	            LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
	            LayoutParams.TYPE_SYSTEM_OVERLAY,
	            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
	                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
	            PixelFormat.RGBA_8888);

	    wm.addView(orientationChanger, orientationLayout);
	    orientationChanger.setVisibility(View.GONE);

	    orientationLayout.screenOrientation = orientation;
	    wm.updateViewLayout(orientationChanger, orientationLayout);
	    orientationChanger.setVisibility(View.VISIBLE);
	    
	    wm.removeView(orientationChanger);
	}
	
	public static void setAutoOrientation(Context context, boolean enabled){
		Settings.System.putInt(context.getContentResolver(),
				Settings.System.ACCELEROMETER_ROTATION, enabled ? 1 : 0);
	}
	
	public static void removeAlarm(Context ctx, int intentId) {
		Intent intent = new Intent(ctx,
				RotationAlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(
				ctx, intentId, intent, 0);
		pendingIntent.cancel();
		AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(pendingIntent);
	}
	
	public static void setAlarm(Context ctx, Long timeInMillis, int orientation, int intentId) {
		Intent intent = new Intent(ctx,
				RotationAlarmReceiver.class);
		intent.putExtra(RotationAlarmReceiver.ORIENTATION, orientation);
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(
				ctx, intentId, intent, 0);
		AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC,
				timeInMillis, 60 * 60 * 24 * 1000,
				pendingIntent);
	}

}
