package bed.rotate.locker;

import android.os.Bundle;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class OptionsActivity extends SherlockFragmentActivity implements AlarmDialogFragment.DialogConfirmListener{
	private ListView mLstRotateAlerts;
	private RotateAlarmsAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_options);

		
		//ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		//ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		//ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
		//ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;


		DataStore settings = DataStore.getInstance(getBaseContext());

		mLstRotateAlerts = (ListView) findViewById(R.id.lstRotateAlerts);

		mAdapter = new RotateAlarmsAdapter(this);

		for (RotateAlarm alarm : settings.getAlarms()) {
			mAdapter.add(alarm);
		}

		mLstRotateAlerts.setAdapter(mAdapter);

	}



	private void showDialog(){
		AlarmDialogFragment df = new AlarmDialogFragment();
		df.show(getSupportFragmentManager(), "dialog");
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.options, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_alarm:
			showDialog();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfirmDialog() {
		mAdapter.clear();
		DataStore settings = DataStore.getInstance(getBaseContext());
		for (RotateAlarm alarm : settings.getAlarms()) {
			mAdapter.add(alarm);
		}
		mAdapter.notifyDataSetChanged();
	}

}
