package bed.rotate.locker;

import org.json.JSONException;
import org.json.JSONObject;

public class RotateAlarm {
	private static final String ALARM_TIME = "alarm_time";
	private static final String INTENT_ID = "intent_id";
	private static final String ORIENTATION_ID = "orientation_id";
	public final static int ORIENTATION_AUTO = 0;
	public final static int ORIENTATION_LANDSCAPE = 1;
	public final static int ORIENTATION_LANDSCAPE_REVERSE = 2;
	public final static int ORIENTATION_PORTRAIT = 3;
	public final static int ORIENTATION_PORTRAIT_REVERSE = 4;
	public final static int ORIENTATION_FIXED = 5;
	
	
	public int alarmIntentId;
	public long timeInMillis;
	public int orientation;
	
	
	public static RotateAlarm parseJson(JSONObject json){
		RotateAlarm alarm = new RotateAlarm();
		try {
			alarm.alarmIntentId = json.getInt(INTENT_ID);
			alarm.orientation = json.getInt(ORIENTATION_ID);
			alarm.timeInMillis = json.getLong(ALARM_TIME);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		return alarm;
	}
	
	public JSONObject toJson(){
		JSONObject result = new JSONObject();
		try {
			result.put(INTENT_ID, alarmIntentId);
			result.put(ORIENTATION_ID, orientation);
			result.put(ALARM_TIME, timeInMillis);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return result;
	}
}
