package bed.rotate.locker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

public class DataStore {
	private static final String OPTIONS_PREFERENCES = "options";
	private static final String ALARMS_OPTION = "alarms";
	private static DataStore instance = null;
	private SharedPreferences prefs;
	private Random rand = null;
	
	private DataStore(Context ctx){
		prefs = ctx.getSharedPreferences(OPTIONS_PREFERENCES, Context.MODE_PRIVATE);
		rand = new Random();
	}
	
	
	public static DataStore getInstance(Context context){
		if(instance == null){
			instance = new DataStore(context);
		}
		return instance;
	}
	
	
	public List<RotateAlarm> getAlarms(){
		String alarmsString = prefs.getString(ALARMS_OPTION, "[]");
		List<RotateAlarm> result = new ArrayList<RotateAlarm>();
		try {
			JSONArray jsAlarmsArray = new JSONArray(alarmsString);
			JSONObject jsAlarm;
			RotateAlarm alarm;
			for (int i = 0; i < jsAlarmsArray.length(); ++i) {
				jsAlarm = jsAlarmsArray.getJSONObject(i);
				alarm = RotateAlarm.parseJson(jsAlarm);
				if(alarm != null){
					result.add(alarm);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void storeAlarms(List<RotateAlarm> alarms){
		JSONArray newAlarms = new JSONArray();
		for (RotateAlarm rotateAlarm : alarms) {
			newAlarms.put(rotateAlarm.toJson());
		}
		prefs.edit()
		.putString(ALARMS_OPTION, newAlarms.toString())
		.commit();
	}


	public int getNextId() {
		//Search new free id
		List<RotateAlarm> alarms = getAlarms();
		List<Integer> alarmIds = new ArrayList<Integer>(alarms.size());
		for (RotateAlarm alarm : alarms) {
			alarmIds.add(alarm.alarmIntentId);
		}
		int nextInt = -1;
		while(nextInt == -1 || alarmIds.contains(nextInt)){
			nextInt = rand.nextInt(Integer.MAX_VALUE) + 1;
		}
		return nextInt;
	}
}
